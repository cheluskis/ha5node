# Crear tres endpoints:

Crear tres endpoints:
- GET /teams que responda con todos los equipos.
Como segunda etapa, que pueda recibir en formato de query strings, tres argumentos:
sortBy, order, skip; de forma tal que la response se ajuste a dichos parámetros.
 - GET /teams/:code que responda con el equipo correspondiente.
Como segunda etapa, que pueda recibir dos query strings: include, exclude que
incluyan o excluyan atributos del equipo.
 - POST /teams que cree el equipo que incluya el body de la request.




### Build & run
```sh
$ npm install
$ npm run dev
```