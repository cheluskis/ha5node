const express = require('express');
const Team = require('./models');

module.exports = (app) => {
    const jsonParser = express.json();

    app.get('/teams', (req, res) => {
        const { sortBy, skip } = req.query;
        let { exclude, include } = req.query;
        const order = req.query.order == -1 ? -1 : 1;
        const opts = {};
        opts.sort = sortBy ? { [sortBy]: order } : {};
        opts.skip = parseInt(skip, 10) || 0;

        exclude = Array.isArray(exclude) ? `${exclude.join(' -')}` : exclude;
        include = Array.isArray(include) ? include.join(' ') : include;

        exclude = exclude ? `-${exclude}` : '';

        console.log(`${include || ''} ${exclude}`);
        Team.find()
            .select(`${include || ''} ${exclude}`)
            .setOptions(opts)
            .then(teams => res.json(teams))
            .catch(error =>
                res.status(500).send({ error, })
            );
    });

    app.get('/teams/:code', (req, res) => {

        let { exclude, include } = req.query;

        exclude = Array.isArray(exclude) ? `${exclude.join(' -')}` : exclude;
        include = Array.isArray(include) ? include.join(' ') : include;

        exclude = exclude ? `-${exclude}` : '';
        console.log(`${include || ''} ${exclude}`);

        const team = Team.findOne({ code: req.params.code }).select(`${include || ''} ${exclude}`);
        team.then(t => {
            if (t) {
                res.json(t);
            } else {
                res.status(404).json({ error: 'Equipo no encontrado' });
            }
        }).catch(error => res.status(500).send({ error }));

    });

    app.post('/teams', jsonParser, (req, res) => {
        if (!req.body.name) {
            return res.status(400).json({ error: 'Nombre de equipo requerido' });
        }
        if (req.body.code.length !== 2) {
            return res.status(400).json({ error: 'Codigo de Equipo debe ser 2 caracteres' });
        }

        const team = new Team(req.body);
        return team.save().then(t => res.json(t));
    });

    app.delete('/teams/:code', (req, res) => {

        const teamQ = Team.findOneAndRemove({ code: req.params.code });
        console.log(teamQ);
        teamQ.then(t => {
            if (t) {
                res.json(t);
            } else {
                res.status(404).json({ error: 'Equipo no encontrado' });
            }
        }).catch(error => res.status(500).send({ error }));

    });

    app.patch('/teams/:code', jsonParser, async (req, res) => {
        console.log(req.params);
        const teamQ = Team.findOneAndUpdate(
            { code: req.params.code },
            req.body
        ).setOptions({ new: true });

        teamQ.then(t => {
            if (t) {
                console.log(t);
                res.json(t);
            } else {
                res.status(404).json({ error: 'Equipo no encontrado' });
            }
        }).catch(error => res.status(500).send({ error }));

    });

    // app.patch('/teams/:code/goal', async (req, res) => {
    //     try {
    //         const team = await Team.findOneAndUpdate({ code: req.params.code }, { $inc: { goal: 1 } });

    //         if (team) {
    //             res.json(team);
    //         } else {
    //             res.status(404).json({ error: 'Equipo no encontrado' });
    //         }

    //     } catch (error) {
    //         res.status(500).send({ error });
    //     }

    // });

    app.post('/teams/:code/goal', jsonParser, async (req, res) => {
        try {
            const { author, against, minute } = req.body;
            const team = await Team.findOneAndUpdate(
                { code: req.params.code },
                { $push: { goals: { author, against, minute } } },
                { new: true }
            );
            if (team) {
                res.json(team);
            } else {
                res.status(404).json({ error: 'Equipo no encontrado' });;
            }
        } catch (error) {
            res.status(500).send({ error });
        }
    });

    app.delete('/teams/:code/goal', jsonParser, async (req, res) => {

        try {
            const team = await Team.findOneAndUpdate(
                { code: req.params.code },
                { $pull: { goals: req.body } },
                { new: true },
            );
            if (team) {
                res.json(team);
            } else {
                res.status(404).json({ error: 'Equipo no encontrado' });;
            }
        } catch (error) {
            res.status(500).send({ error });
        }
    });

    app.put('/teams/:code/goal', jsonParser, async (req, res) => {

        try {
            const { author: authora, against: againsta, minute: minutea } = req.body.after;

            const team = await Team.findOneAndUpdate(
                {
                    code: req.params.code, goals: { $elemMatch: req.body.before }
                },
                { $set: { 'goals.$.author': authora, 'goals.$.against': againsta, 'goals.$.minute': minutea } },
                { new: true },
            );
            if (team) {
                res.json(team);
            } else {
                res.status(404).json({ error: 'Equipo no encontrado' });;
            }
        } catch (error) {
            res.status(500).send({ error });
        }
    });

    app.get('/teams/:code/goal/last', jsonParser, async (req, res) => {

        try {
            const team = await Team.findOne({ code: req.params.code });
            console.log(team);
            if (team) {
                res.json(team.goals.pop());
            } else {
                res.status(404).json({ error: 'Equipo no encontrado' });;
            }
        } catch (error) {
            res.status(500).send({ error });
        }
    });


    app.delete('/teams/:code/goal/last', async (req, res) => {

        try {
            const team = await Team.findOneAndUpdate(
                { code: req.params.code },
                { $pop: { goals: 1 } }
            );
            if (team) {
                res.json(team);
            } else {
                res.status(404).json({ error: 'Equipo no encontrado' });;
            }
        } catch (error) {
            res.status(500).send({ error });
        }
    });


};
