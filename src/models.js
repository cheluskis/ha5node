const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/clase5', { useNewUrlParser: true });

mongoose.connection
    .once('open', () => console.log('Conexión con la base de datos establecida!'))
    .on('error', error => console.log(error));



const GoalSchema = new mongoose.Schema({
    author: String,
    against: String,
    minute: Number,
});

const TeamSchema = new mongoose.Schema({
    code: String,
    name: String,
    flag: String,
    goals: [GoalSchema]
});

const Team = mongoose.model('Team', TeamSchema);

module.exports = Team;
